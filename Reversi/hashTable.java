package Reversi;

import Reversi.hashEntry;
import java.util.UUID;
import java.io.*;

public class hashTable {
	private static int TABLE_SIZE = 20000;

	public hashEntry[] table;
	public long[][][] zobristTable;

	public hashTable() {
		this.table = new hashEntry[TABLE_SIZE];
		this.zobristTable = new long[reversi.SIZE][reversi.SIZE][3];
	}

	public void zobrist_init() {
		this.zobristTable = new long[reversi.SIZE][reversi.SIZE][3];
		int l = 0;
		for(int i = 0; i < reversi.SIZE; i++) {
			for(int j = 0; j < reversi.SIZE; j++) {
				for(int k = 0; k < 3; k++) {
					UUID uuid = UUID.randomUUID();
					long key = uuid.getLeastSignificantBits();
					this.zobristTable[i][j][k] = key;
				}
			}
		}
	}

	public long zobrist(position[][] board) {
		long temp = 0;
		int thirdIndex = 0;
		for(int i = 0; i < reversi.SIZE; i++) {
			for(int j = 0; j < reversi.SIZE; j++) {
				if(board[i][j].piece == reversi.pieces.EMPTY) {
					thirdIndex = 0;
				} else if(board[i][j].piece == reversi.pieces.BLACK) {
					thirdIndex = 1;
				} else if(board[i][j].piece == reversi.pieces.WHITE) {
					thirdIndex = 2;
				}
				temp ^= this.zobristTable[i][j][thirdIndex];
			}
		}
		return temp;
	}

	public int hash(long zobrist) {
		int key = java.lang.Math.abs((int) (zobrist % TABLE_SIZE));
		return key;
	}

	public void nullHash(int hash) {
		this.table[hash] = null;
	}

	public void addPosition(hashEntry newEntry) {
		int hash = hash(newEntry.zobrist);

		hashEntry curr = table[hash];

		if(curr == null) {
			table[hash] = newEntry;
			table[hash].hash = hash;
			//System.out.println("Created new entry at " + hash);
			return;
		} else {
			int i = 0;
			while(table[hash % TABLE_SIZE] != null && i < TABLE_SIZE) {
				//System.out.println(hash);
				hash++;
				i++;
			}
			table[hash % TABLE_SIZE] = newEntry;
			table[hash % TABLE_SIZE].hash = hash;
			//System.out.println("Created new entry at " + hash % TABLE_SIZE);
			return;
		}
	}

	public hashEntry findPosition(long zobrist) {
		int hash = hash(zobrist);

		if(table[hash % TABLE_SIZE] == null) {
			return null;
		} else {
			int i = 0;
			while(i < TABLE_SIZE) {
				if(table[hash % TABLE_SIZE] != null) {
					if(table[hash % TABLE_SIZE].zobrist == zobrist) {
						break;
					} else {
						hash++;
						i++;
					}
				} else {
					hash++;
					i++;
				}
			}
			if(i == TABLE_SIZE) {
				return null;
			} else {
				return table[hash % TABLE_SIZE];
			}
		}
	}

	public void removePosition(int hash) {
		table[hash] = null;
	}

	public void print() {
		File file = new File("table.txt");
		file.delete();
		for(int i = 0; i < TABLE_SIZE; i++) {
			try(FileWriter fw = new FileWriter("table.txt", true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
			{
				if(table[i] != null) {
					String zobrist = Long.toString(table[i].zobrist);
					String depth = Integer.toString(table[i].depth);
					String flag;
					if(table[i].flag == hashEntry.flags.hashDEFAULT) {
						flag = "default";
					} else if (table[i].flag == hashEntry.flags.hashALPHA) {
						flag = "alpha";
					} else if (table[i].flag == hashEntry.flags.hashBETA) {
						flag = "beta";
					} else {
						flag = "exact";
					}
					String score = Integer.toString(table[i].score);
					String row;
					String col;
					if(table[i].move != null) {
						row = Integer.toString(table[i].move.row);
						col = Integer.toString(table[i].move.col);
					} else {
						row = "-1";
						col = "-1";
					}
					String hash = Integer.toString(table[i].hash);
				    out.println(i + " " + zobrist + " " + depth + " " + flag + " " + score + " " + row + " " + col + " " + hash);
				} else {
					out.println(i + " no entry");
				}
			} catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
		}
		try(FileWriter fw = new FileWriter("table.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw))
		{
			for(int i = 0; i < reversi.SIZE; i++) {
				for(int j = 0; j < reversi.SIZE; j++) {
					for(int k = 0; k < 3; k++) {
						out.println(this.zobristTable[i][j][k]);
					}
				}
			}
		} catch (IOException e) {
			//exception handling left as an exercise for the reader
		}
	}
}
