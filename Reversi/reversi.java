package Reversi;

import Reversi.*;
import java.io.*;
import java.util.Random;

class position {
	reversi.pieces piece;
	position right;
	position left;
	position up;
	position down;
	position rightup;
	position rightdown;
	position leftup;
	position leftdown;
	int row;
	int col;

	position() {
		this.piece = reversi.pieces.EMPTY;
		this.right = null;
		this.left = null;
		this.up = null;
		this.down = null;
		this.rightup = null;
		this.rightdown = null;
		this.leftup = null;
		this.leftdown = null;
		this.row = -1;
		this.col = -1;
	}
}

class moves {
	boolean right;
	boolean left;
	boolean up;
	boolean down;
	boolean rightup;
	boolean rightdown;
	boolean leftup;
	boolean leftdown;

	moves() {
		this.right = false;
		this.left = false;
		this.up = false;
		this.down = false;
		this.rightup = false;
		this.rightdown = false;
		this.leftup = false;
		this.leftdown = false;
	}
}

class place {
	int row;
	int col;
	int score;
	int depth;

	place() {
		this.row = -1;
		this.col = -1;
		this.score = 0;
	}
}

class counts {
	public int zCount;
	public int mCount;
	public int rCount;
	public int bCount;
	public int eCount;

	public counts() {
		this.zCount = 0;
		this.mCount = 0;
		this.rCount = 0;
		this.bCount = 0;
		this.eCount = 0;
	}
}

public class reversi {
	public enum pieces {
		WHITE, BLACK, EMPTY;
	}
	public enum direction {
		RIGHT, LEFT, UP, DOWN, FAIL;
	}
	public static final int SIZE = 8;
	public static final int MAX_DEPTH = 6;
	public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RESET = "\u001B[0m";

	public static hashTable loadTable() {
		hashTable table = new hashTable();
		try {
			File file = new File("table.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String line;
			String params[];
			int hash;
			long zobrist;
			int depth;
			hashEntry.flags flag;
			int score;
			int row;
			int col;
			long[] zobristTable = new long[SIZE * SIZE * 3];
			int i = 0;
			while((line = br.readLine()) != null) {
				params = line.split(" ");
				if(params.length == 8) {
					hash = Integer.valueOf(params[0]);
					zobrist = Long.valueOf(params[1]);
					depth = Integer.valueOf(params[2]);
					if(params[3] == "exact") {
						flag = hashEntry.flags.hashEXACT;
					} else if(params[3] == "default") {
						flag = hashEntry.flags.hashDEFAULT;
					} else if(params[3] == "alpha") {
						flag = hashEntry.flags.hashALPHA;
					} else {
						flag = hashEntry.flags.hashBETA;
					}
					score = Integer.valueOf(params[4]);
					row = Integer.valueOf(params[5]);
					col = Integer.valueOf(params[6]);
					hash = Integer.valueOf(params[7]);
					hashBestMove bestMove = new hashBestMove(row,col);
					hashEntry temp = new hashEntry(zobrist,depth,flag,score,bestMove);
					table.addPosition(temp);
				} else if(params.length == 3) {
					hash = Integer.valueOf(params[0]);
					table.nullHash(hash);
				} else if(params.length == 1) {
					zobrist = Long.valueOf(params[0]);
					zobristTable[i] = zobrist;
					i++;
				}
			}
			int l = 0;
			for(i = 0; i < SIZE; i++) {
				for(int j = 0; j < SIZE; j++) {
					for(int k = 0; k < 3; k++) {
						table.zobristTable[i][j][k] = zobristTable[l];
						l++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintStream(System.out));
			return null;
		}
		return table;
	}

	public static position[][] setBoard(position[][] board, boolean empty) {
		if(!empty) {
			board[SIZE / 2 - 1][SIZE / 2 - 1].piece = pieces.WHITE;
			board[SIZE / 2 - 1][SIZE / 2].piece = pieces.BLACK;
			board[SIZE / 2][SIZE / 2 - 1].piece = pieces.BLACK;
			board[SIZE / 2][SIZE / 2].piece = pieces.WHITE;
		}

		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				board[i][j].row = i;
				board[i][j].col = j;
				if(j < SIZE - 1) {
					board[i][j].right = board[i][j+1];
				}
				if(j > 0) {
					board[i][j].left = board[i][j-1];
				}
				if(i < SIZE - 1) {
					board[i][j].down = board[i+1][j];
				}
				if(i > 0) {
					board[i][j].up = board[i-1][j];
				}
				if(i > 0 && j > 0) {
					board[i][j].leftup = board[i-1][j-1];
				}
				if(i > 0 && j < SIZE - 1) {
					board[i][j].rightup = board[i-1][j+1];
				}
				if(i < SIZE - 1 && j > 0) {
					board[i][j].leftdown = board[i+1][j-1];
				}
				if(i < SIZE - 1 && j < SIZE - 1) {
					board[i][j].rightdown = board[i+1][j+1];
				}
			}
		}

		return board;
	}

	public static void printBoard(position[][] board) {
		System.out.print("    ");
		for(int i = 0; i < SIZE; i++) {
			System.out.print((char) (i + 65) + "   ");
		}
		System.out.println();
		for(int i = 0; i < SIZE; i++) {
			System.out.print("  ");
			for(int j = 0; j < SIZE; j++) {
				System.out.print(ANSI_GREEN_BACKGROUND + "+---" + ANSI_RESET);
			}
			System.out.println(ANSI_GREEN_BACKGROUND + "+" + ANSI_RESET);
			System.out.print(i + 1 + " ");
			for(int j = 0; j < SIZE; j++) {
				if(board[i][j].piece == pieces.WHITE) {
					System.out.print(ANSI_GREEN_BACKGROUND + "| " + ANSI_WHITE + "O " + ANSI_RESET);
				} else if(board[i][j].piece == pieces.BLACK) {
					System.out.print(ANSI_GREEN_BACKGROUND + "| " + ANSI_BLACK + "O " + ANSI_RESET);
				} else {
					System.out.print(ANSI_GREEN_BACKGROUND + "|   " + ANSI_RESET);
				}
			}
			System.out.println(ANSI_GREEN_BACKGROUND + "|" + ANSI_RESET);
		}
		System.out.print("  ");
		for(int j = 0; j < SIZE; j++) {
			System.out.print(ANSI_GREEN_BACKGROUND + "+---" + ANSI_RESET);
		}
		System.out.println(ANSI_GREEN_BACKGROUND + "+" + ANSI_RESET);
	}

	public static moves findFlip(position[][] board, int row, int col, pieces player) {
		moves flip = new moves();
		int count = 0;
		pieces flipping = pieces.EMPTY;
		if(player == pieces.WHITE) {
			flipping = pieces.BLACK;
		} else {
			flipping = pieces.WHITE;
		}

		//System.out.println("row: " + row + " col: " + col);
		position curr = board[row][col].right;
		while(curr != null) {
			//System.out.println("right");
			//System.out.println("row: " + curr.row + " col: " + curr.col);
			if(curr.piece != flipping) {
				//System.out.println("end");
				if(curr.piece == player && count > 0) {
					//System.out.println("found");
					flip.right = true;
				}
				break;
			}
			curr = curr.right;
			count++;
		}
		curr = board[row][col].left;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.left = true;
				}
				break;
			}
			curr = curr.left;
			count++;
		}
		curr = board[row][col].up;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.up = true;
				}
				break;
			}
			curr = curr.up;
			count++;
		}
		curr = board[row][col].down;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.down = true;
				}
				break;
			}
			curr = curr.down;
			count++;
		}
		curr = board[row][col].leftdown;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.leftdown = true;
				}
				break;
			}
			curr = curr.leftdown;
			count++;
		}
		curr = board[row][col].leftup;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.leftup = true;
				}
				break;
			}
			curr = curr.leftup;
			count++;
		}
		curr = board[row][col].rightdown;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.rightdown = true;
				}
				break;
			}
			curr = curr.rightdown;
			count++;
		}
		curr = board[row][col].rightup;
		count = 0;
		while(curr != null) {
			if(curr.piece != flipping) {
				if(curr.piece == player && count > 0) {
					flip.rightup = true;
				}
				break;
			}
			curr = curr.rightup;
			count++;
		}

		return flip;
	}

	public static position[][] placePiece(position[][]board, int row, int col, pieces player) {
		board[row][col].piece = player;
		position curr = board[row][col];
		moves flip = findFlip(board, row, col, player);
		if(flip.right) {
			curr = curr.right;
			while(curr.piece != player && curr.piece != pieces.EMPTY) {
				if(player == pieces.WHITE) {
					curr.piece = pieces.WHITE;
				} else {
					curr.piece = pieces.BLACK;
				}
				curr = curr.right;
			}
		}
		curr = board[row][col];
		if(flip.left) {
			curr = curr.left;
			while(curr.piece != player && curr.piece != pieces.EMPTY) {
				if(player == pieces.WHITE) {
					curr.piece = pieces.WHITE;
				} else {
					curr.piece = pieces.BLACK;
				}
				curr = curr.left;
			}
		}
		curr = board[row][col];
		if(flip.up) {
			curr = curr.up;
			while(curr.piece != player && curr.piece != pieces.EMPTY) {
				if(player == pieces.WHITE) {
					curr.piece = pieces.WHITE;
				} else {
					curr.piece = pieces.BLACK;
				}
				curr = curr.up;
			}
		}
		curr = board[row][col];
		if(flip.down) {
			curr = curr.down;
			while(curr.piece != player && curr.piece != pieces.EMPTY) {
				if(player == pieces.WHITE) {
					curr.piece = pieces.WHITE;
				} else {
					curr.piece = pieces.BLACK;
				}
				curr = curr.down;
			}
		}
		curr = board[row][col];
		if(flip.leftdown) {
		   curr = curr.leftdown;
		   while(curr.piece != player && curr.piece != pieces.EMPTY) {
			   if(player == pieces.WHITE) {
				   curr.piece = pieces.WHITE;
			   } else {
				   curr.piece = pieces.BLACK;
			   }
			   curr = curr.leftdown;
		   }
		}
		curr = board[row][col];
		if(flip.leftup) {
		   curr = curr.leftup;
		   while(curr.piece != player && curr.piece != pieces.EMPTY) {
			   if(player == pieces.WHITE) {
				   curr.piece = pieces.WHITE;
			   } else {
				   curr.piece = pieces.BLACK;
			   }
			   curr = curr.leftup;
		   }
		}
		curr = board[row][col];
		if(flip.rightdown) {
		   curr = curr.rightdown;
		   while(curr.piece != player && curr.piece != pieces.EMPTY) {
			   if(player == pieces.WHITE) {
				   curr.piece = pieces.WHITE;
			   } else {
				   curr.piece = pieces.BLACK;
			   }
			   curr = curr.rightdown;
		   }
		}
		curr = board[row][col];
		if(flip.rightup) {
		   curr = curr.rightup;
		   while(curr.piece != player && curr.piece != pieces.EMPTY) {
			   if(player == pieces.WHITE) {
				   curr.piece = pieces.WHITE;
			   } else {
				   curr.piece = pieces.BLACK;
			   }
			   curr = curr.rightup;
		   }
		}
		return board;
	}

	public static position[][] playerMove(position[][] board) {
		System.out.print("Player 1, enter move(col row, A1): ");
		int row = 0, col = 0;
		moves flip = new moves();
		while(true) {
			String move = System.console().readLine();
			col = move.charAt(0) - 65;
			row = move.charAt(1) - 49;
			if(board[row][col].piece == pieces.EMPTY) {
				flip = findFlip(board, row, col, pieces.BLACK);
				if(flip.right || flip.left || flip.up || flip.down || flip.leftup || flip.leftdown || flip.rightup || flip.rightdown) {
					break;
				} else {
					System.out.print("Invalid move, try again: ");
				}
			} else {
				System.out.print("Spot is not empty, try again: ");
			}
		}

		board = placePiece(board,row,col,pieces.BLACK);

		return board;
	}

	public static hashTable addHashEntry(position[][] board, place move, hashTable table) {
		long zobrist = table.zobrist(board);
		hashEntry temp = table.findPosition(zobrist);
		if(temp == null) {
			//System.out.print("a");
			//add entry
			hashBestMove bestMove = new hashBestMove();
			bestMove.row = move.row;
			bestMove.col = move.col;
			temp = new hashEntry(zobrist,move.depth,hashEntry.flags.hashEXACT,move.score,bestMove);
			table.addPosition(temp);
			return table;
		} else {
			if(temp.depth < move.depth) {
				//System.out.print("r");
				//replace entry
				table.removePosition(temp.hash);
				hashBestMove bestMove = new hashBestMove();
				bestMove.row = move.row;
				bestMove.col = move.col;
				temp = new hashEntry(zobrist,move.depth,hashEntry.flags.hashEXACT,move.score,bestMove);
				table.addPosition(temp);
				return table;
			} else {
				return table;
			}
		}
	}

	public static place analyze(position[][] board, pieces player, int move, hashTable table, counts counter, boolean isMaximizingPlayer, int alpha, int beta) {
		if(move > MAX_DEPTH) {
			//System.out.print("m");
			counter.mCount++;
			place bestMove = new place();
			bestMove.row = -1;
			bestMove.col = -1;
			bestMove.score = 0;
			return bestMove;
		}

		long zobrist = table.zobrist(board);
		hashEntry finder = table.findPosition(zobrist);
		if(finder != null) {
			//System.out.print("n");
			if(finder.move != null && finder.depth > 3) {
				//System.out.print("m");
				if(finder.move.row >= 0 && finder.move.col >= 0) {
					//System.out.print("z");
					counter.zCount++;
					place bestMove = new place();
					bestMove.row = finder.move.row;
					bestMove.col = finder.move.col;
					bestMove.score = finder.score;
					return bestMove;
				}
			}
		}
		//System.out.println(move);
		position[][] workingBoard = board;
		// if no more moves, return difference in scores
		if(endGame(board,player)) {
			int white = 0, black = 0;
			for(int i = 0; i < SIZE; i++) {
				for(int j = 0; j < SIZE; j++) {
					if(workingBoard[i][j].piece == pieces.WHITE) {
						white++;
					} else if(workingBoard[i][j].piece == pieces.BLACK) {
						black++;
					}
				}
			}
			place endTurn = new place();
			if(player == pieces.WHITE) {
				endTurn.score = white - black;
			} else {
				endTurn.score = black - white;
			}
			counter.eCount++;
			return endTurn;
		}

		//printBoard(workingBoard);

		place[] emptySpots = new place[SIZE * SIZE];
		for(int i = 0; i < SIZE * SIZE; i++) {
			emptySpots[i] = new place();
		}
		int k = 0;

		moves flip = new moves();
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				flip = findFlip(workingBoard, i, j, player);
				if(workingBoard[i][j].piece == pieces.EMPTY) {
					if(flip.right || flip.left || flip.up || flip.down || flip.leftup || flip.leftdown || flip.rightup || flip.rightdown) {
						/*System.out.println("Possible row: " + i + " col: " + j);
						if(flip.right) {
							System.out.print("r");
						}
						if(flip.left) {
							System.out.print("l");
						}
						if(flip.up) {
							System.out.print("u");
						}
						if(flip.down) {
							System.out.print("d");
						}
						if(flip.leftup) {
							System.out.print("ru");
						}
						if(flip.leftdown) {
							System.out.print("rd");
						}
						if(flip.rightup) {
							System.out.print("ru");
						}
						if(flip.rightdown) {
							System.out.print("rd");
						}
						System.out.println();*/
						emptySpots[k].row = i;
						emptySpots[k].col = j;
						k++;
					}
				}
			}
		}

		place turns[] = new place[SIZE * SIZE];
		for(int i = 0; i < SIZE * SIZE; i++) {
			turns[i] = new place();
		}

		place aiTurn = new place();
		place aiMoves = new place();
		int bestScore = -10000;
		int worstScore = 10000;
		int bestMove = 0;
		for(int i = 0; i < k; i++) {
			aiTurn = emptySpots[i];
			workingBoard = placePiece(workingBoard,aiTurn.row,aiTurn.col,player);
			if(player == pieces.WHITE) {
				aiMoves = analyze(workingBoard,pieces.BLACK,move+1,table,counter,!isMaximizingPlayer,alpha,beta);
				aiTurn.score += aiMoves.score;
				//addHashEntry(workingBoard, aiMoves, table);
				if((aiTurn.row == SIZE - 1 && aiTurn.col == SIZE - 1) ||
				(aiTurn.row == SIZE - 1 && aiTurn.col == 0) ||
				(aiTurn.row == 0 && aiTurn.col == SIZE - 1) ||
				(aiTurn.row == 0 && aiTurn.col == 0)) {
					aiTurn.score += 5;
				} else if((aiTurn.row == 1 && aiTurn.col == 1) ||
					(aiTurn.row == 1 && aiTurn.col == SIZE - 2) ||
					(aiTurn.row == SIZE - 2 && aiTurn.col == 1) ||
					(aiTurn.row == SIZE - 2 && aiTurn.col == SIZE - 2) ||
					(aiTurn.row == 0 && (aiTurn.col == 1 || aiTurn.col == SIZE - 2)) ||
					(aiTurn.row == SIZE - 1 && (aiTurn.col == 1 || aiTurn.col == SIZE - 2)) ||
					(aiTurn.col == 0 && (aiTurn.row == 1 || aiTurn.row == SIZE - 2)) ||
					(aiTurn.col == SIZE - 1 && (aiTurn.row == 1 || aiTurn.col == SIZE - 2)) ){
						aiTurn.score -= 15;
				} else if (aiTurn.row == SIZE - 1 || aiTurn.row == 0 || aiTurn.col == SIZE - 1 || aiTurn.col == 0) {
					aiTurn.score += 1;
				}
				workingBoard[aiTurn.row][aiTurn.col].piece = pieces.EMPTY;
				turns[i] = aiTurn;
				if(isMaximizingPlayer) {
					if(turns[i].score > bestScore) {
						bestScore = turns[i].score;
						bestMove = i;
					}
					if(bestScore > alpha) {
						alpha = bestScore;
					}
					if(beta <= alpha) {
						break;
					}
				} else {
					if(turns[i].score < worstScore) {
						worstScore = turns[i].score;
						bestMove = i;
					}
					if(worstScore < beta) {
						beta = worstScore;
					}
					if(beta <= alpha) {
						break;
					}
				}
			} else {
				aiMoves = analyze(workingBoard,pieces.WHITE,move+1,table,counter,!isMaximizingPlayer,alpha,beta);
				aiTurn.score += aiMoves.score;
				//addHashEntry(workingBoard, aiMoves, table);
				if((aiTurn.row == SIZE - 1 && aiTurn.col == SIZE - 1) ||
				(aiTurn.row == SIZE - 1 && aiTurn.col == 0) ||
				(aiTurn.row == 0 && aiTurn.col == SIZE - 1) ||
				(aiTurn.row == 0 && aiTurn.col == 0)) {
					aiTurn.score += 5;
				} else if((aiTurn.row == 1 && aiTurn.col == 1) ||
					(aiTurn.row == 1 && aiTurn.col == SIZE - 2) ||
					(aiTurn.row == SIZE - 2 && aiTurn.col == 1) ||
					(aiTurn.row == SIZE - 2 && aiTurn.col == SIZE - 2) ||
					(aiTurn.row == 0 && (aiTurn.col == 1 || aiTurn.col == SIZE - 2)) ||
					(aiTurn.row == SIZE - 1 && (aiTurn.col == 1 || aiTurn.col == SIZE - 2)) ||
					(aiTurn.col == 0 && (aiTurn.row == 1 || aiTurn.row == SIZE - 2)) ||
					(aiTurn.col == SIZE - 1 && (aiTurn.row == 1 || aiTurn.col == SIZE - 2)) ){
						aiTurn.score -= 15;
				} else if (aiTurn.row == SIZE - 1 || aiTurn.row == 0 || aiTurn.col == SIZE - 1 || aiTurn.col == 0) {
					aiTurn.score += 1;
				}
				workingBoard[aiTurn.row][aiTurn.col].piece = pieces.EMPTY;
				turns[i] = aiTurn;
				if(isMaximizingPlayer) {
					if(turns[i].score > bestScore) {
						bestScore = turns[i].score;
						bestMove = i;
					}
					if(bestScore > alpha) {
						alpha = bestScore;
					}
					if(beta <= alpha) {
						break;
					}
				} else {
					if(turns[i].score < worstScore) {
						worstScore = turns[i].score;
						bestMove = i;
					}
					if(worstScore < beta) {
						beta = worstScore;
					}
					if(beta <= alpha) {
						break;
					}
				}
			}
		}
		turns[bestMove].depth = move;
		addHashEntry(workingBoard, turns[bestMove], table);
		counter.bCount++;
		return turns[bestMove];
	}

	public static position[][] aiMove(position[][] board, pieces player, hashTable table, counts counter) {
		position[][] temp = new position[SIZE][SIZE];
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				temp[i][j] = new position();
				temp[i][j].piece = board[i][j].piece;
			}
		}
		setBoard(temp,true);

		place ai = analyze(temp, player,1,table,counter,true,-100000,100000);
		//System.out.println("Best Score: " + ai.score);
		table = addHashEntry(board,ai,table);

		if(ai.row < 0 || ai.col < 0) {
			if(player == pieces.WHITE) {
				System.out.println("No possible white move");
			} else {
				System.out.println("No possible black move");
			}
			return board;
		}

		board = placePiece(board,ai.row,ai.col,player);

		//System.out.println("AI move: " + (char)(ai.col + 65) + (ai.row + 1));
		return board;
	}

	public static position[][] randMove(position[][] board, pieces player) {
		place[] emptySpots = new place[SIZE * SIZE];
		for(int i = 0; i < SIZE * SIZE; i++) {
			emptySpots[i] = new place();
		}
		int k = 0;

		moves flip = new moves();
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				flip = findFlip(board, i, j, player);
				if(board[i][j].piece == pieces.EMPTY) {
					if(flip.right || flip.left || flip.up || flip.down || flip.leftup || flip.leftdown || flip.rightup || flip.rightdown) {
						emptySpots[k].row = i;
						emptySpots[k].col = j;
						k++;
					}
				}
			}
		}

		Random rand = new Random();
		int move = rand.nextInt(k);
		board = placePiece(board,emptySpots[move].row,emptySpots[move].col,player);
		return board;
	}

	public static void win(position[][] board, hashTable table, counts counter) {
		int white = 0, black = 0;
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				if(board[i][j].piece == pieces.WHITE) {
					white++;
				} else if(board[i][j].piece == pieces.BLACK) {
					black++;
				}
			}
		}
		if(white > black) {
			System.out.println("Player 2 wins!");
		} else if(black > white) {
			System.out.println("Player 1 wins!");
		} else {
			System.out.println("It's a tie!");
		}
		System.out.println("Score: " + black + "-" + white);
		System.out.println("Zobrists: " + counter.zCount);
		System.out.println("Max moves: " + counter.mCount);
		System.out.println("End games: " + counter.eCount);
		System.out.println("Randoms: " + counter.rCount);
		System.out.println("Bests: " + counter.bCount);
		table.print();
		try(FileWriter fw = new FileWriter("zobristcount.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw))
		{
			out.println(counter.zCount);
		} catch (IOException e) {
			//exception handling left as an exercise for the reader
		}
		System.exit(1);
	}

	public static boolean endGame(position[][] board, pieces player) {
		moves flip = new moves();
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				if(board[i][j].piece == pieces.EMPTY) {
					flip = findFlip(board, i, j, player);
					if(flip.right || flip.left || flip.up || flip.down || flip.leftup || flip.leftdown || flip.rightup || flip.rightdown) {
						if(flip.right) {
							//System.out.println("Found right: row " + i + " col " + j + " " + player);
						} else if(flip.left) {
							//System.out.println("Found left: row " + i + " col " + j + " " + player);
						} else if(flip.up) {
							//System.out.println("Found up: row " + i + " col " + j + " " + player);
						} else if(flip.down) {
							//System.out.println("Found down: row " + i + " col " + j + " " + player);
						}
						return false;
					}
				}
			}
		}
		//System.out.println("No moves found");
		return true;
	}

	public static void main(String[] args) {
		int filled = 0;
		System.out.println("Welcome to Reversi");
		counts counter = new counts();
		position[][] board;
		board = new position[SIZE][SIZE];
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				board[i][j] = new position();
			}
		}
		board = setBoard(board, false);
		hashTable table = loadTable();
		if(table == null) {
			System.out.println("No table");
			table = new hashTable();
			table.zobrist_init();
			hashEntry newEntry = new hashEntry(table.zobrist(board),0,hashEntry.flags.hashDEFAULT,0,null);
			table.addPosition(newEntry);
		}

		filled = 4;
		boolean humanGameOver = false;
		boolean aiGameOver = false;
		printBoard(board);
		while(filled < SIZE * SIZE + 1) {
			humanGameOver = endGame(board,pieces.BLACK);
			if(!humanGameOver) {
				//board = playerMove(board);
				//board = aiMove(board,pieces.BLACK,table,counter);
				board = randMove(board,pieces.BLACK);
				filled++;
			}
			//printBoard(board);

			aiGameOver = endGame(board,pieces.WHITE);
			if(!aiGameOver) {
				board = aiMove(board,pieces.WHITE,table,counter);
				filled++;
			}
			//printBoard(board);

			humanGameOver = endGame(board,pieces.BLACK);
			aiGameOver = endGame(board,pieces.WHITE);
			if(humanGameOver && aiGameOver) {
				break;
			}
		}
		printBoard(board);
		win(board,table,counter);
	}
}
