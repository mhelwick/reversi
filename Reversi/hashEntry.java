package Reversi;

class hashBestMove {
	public int row;
	public int col;

	public hashBestMove() {
		this.row = -1;
		this.col = -1;
	}

	public hashBestMove(int row, int col) {
		this.row = row;
		this.col = col;
	}
}

class hashEntry {
	enum flags { hashEXACT, hashALPHA, hashBETA, hashDEFAULT }

	public long zobrist;
	public int depth;
	public flags flag;
	public int score;
	public hashBestMove move;
	public int hash;

	public hashEntry(long zobrist, int depth, flags flag, int score, hashBestMove move) {
		this.zobrist = zobrist;
    	this.depth = depth;
    	this.flag = flag;
		this.score = score;
		this.move = move;
		this.hash = -1;
	}
}
